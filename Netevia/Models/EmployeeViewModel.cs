﻿using Netevia.Domain;
using Netevia.Domain.Enums;

namespace Netevia.Models;

public class EmployeeViewModel
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int Age { get; set; }
    public Gender Gender { get; set; }
    public DepartmentViewModel? Department { get; set; }
}

public class AddEmployeeModel
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int Age { get; set; }
    public Gender Gender { get; set; }
    public int? DepartmentId { get; set; }
    public List<int> ExperienceIds { get; set; }
}

public class EditEmployeeViewModel : AddEmployeeModel
{
    public int Id { get; set; }
}