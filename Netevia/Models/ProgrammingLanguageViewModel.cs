﻿namespace Netevia.Models;

public class ProgrammingLanguageViewModel
{
    public int Id { get; set; }
    public string Name { get; set; }
}