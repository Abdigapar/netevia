﻿namespace Netevia.Domain;

/// <summary>
/// Отдел
/// </summary>
public class Department : AuditableBaseEntity<int>
{
    public string Name { get; set; }
    public int Floor { get; set; }
}