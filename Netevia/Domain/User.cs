﻿namespace Netevia.Domain;

public class User : AuditableBaseEntity<int>
{
    public string Username { get; set; }
    public string Password { get; set; }
    public DateTime LastActionTime { get; set; }
}