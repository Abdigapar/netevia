﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Netevia.Domain;

/// <summary>
/// Опыт
/// </summary>
public class Experience : AuditableBaseEntity<int>
{
    public int EmployeeId { get; set; }
    [ForeignKey(nameof(EmployeeId))]
    public Employee Employee { get; set; }

    public int ProgrammingLanguageId { get; set; }
    [ForeignKey(nameof(ProgrammingLanguageId))]
    public ProgrammingLanguage? ProgrammingLanguage { get; set; }
}