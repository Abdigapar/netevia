﻿namespace Netevia.Domain;

public class AuditableBaseEntity<T>
{
    public T Id { get; set; }
    public string? CreatedById { get; set; }

    public DateTime? Created { get; set; }

    public string? CreatedByName { get; set; }

    public string? LastModifiedById { get; set; }

    public DateTime? LastModified { get; set; }

    public string? LastModifiedByName { get; set; }

    public bool IsDelete { get; set; } = false;
}