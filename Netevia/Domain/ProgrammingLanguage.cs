﻿namespace Netevia.Domain;

/// <summary>
/// Язык программирования
/// </summary>
public class ProgrammingLanguage : AuditableBaseEntity<int>
{
    public string Name { get; set; }
}