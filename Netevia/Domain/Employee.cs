﻿using System.ComponentModel.DataAnnotations.Schema;
using Netevia.Domain.Enums;

namespace Netevia.Domain;

/// <summary>
/// Сотрудник
/// </summary>
public class Employee : AuditableBaseEntity<int>
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int Age { get; set; }
    public Gender Gender { get; set; }
    public int? DepartmentId { get; set; }
    [ForeignKey(nameof(DepartmentId))]
    public Department? Department { get; set; }
    
    public virtual ICollection<Experience> Experiences { get; set; }
}