﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Netevia.Authorization;
using Netevia.Features.Interfaces;
using Netevia.Models;

namespace Netevia.Controllers;

[Authorize]
public class EmployeeController : Controller
{
    private readonly IEmployeeService _employeeService;
    private readonly IDepartmentService _departmentService;
    private readonly IProgrammingLanguageService _programmingLanguageService;

    public EmployeeController(
        IEmployeeService employeeService, 
        IDepartmentService departmentService, 
        IProgrammingLanguageService programmingLanguageService)
    {
        _employeeService = employeeService;
        _departmentService = departmentService;
        _programmingLanguageService = programmingLanguageService;
    }

    public async Task<IActionResult> Index(string searchString)
    {
        var employees = await _employeeService.GetAll(searchString);
        return View(employees.ToList());
    }

    [HttpGet]
    public async Task<IActionResult> Add()
    {
        var departments = await _departmentService.GetAll();
        var programmingLanguages = await _programmingLanguageService.GetAll();
        
        ViewBag.Departments = new SelectList(departments, "Id", "Name");
        ViewBag.ProgrammingLanguages = new SelectList(programmingLanguages, "Id", "Name");
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Add(AddEmployeeModel employee)
    {
        if (!ModelState.IsValid) return View(employee);
        await _employeeService.Add(employee);
        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(int id)
    {
        var employee = await _employeeService.GetById(id);
        var departments = await _departmentService.GetAll();
        var programmingLanguages = await _programmingLanguageService.GetAll();
        
        ViewBag.Departments = new SelectList(departments, "Id", "Name", employee.DepartmentId);
        ViewBag.ProgrammingLanguages = new SelectList(programmingLanguages, "Id", "Name", employee.ExperienceIds);
        
        return View(employee);
    }
    
    [HttpPost]
    public async Task<IActionResult> Edit(int id, EditEmployeeViewModel employee)
    {
        if (!ModelState.IsValid) return View(employee);
        await _employeeService.Edit(id, employee);
        return RedirectToAction(nameof(Index));
    }

    public async Task<IActionResult> Delete(int id)
    {
        await _employeeService.Delete(id);
        return RedirectToAction(nameof(Index));
    }
}