﻿using Microsoft.AspNetCore.Mvc;
using Netevia.Features.Interfaces;
using Netevia.Models;

namespace Netevia.Controllers;

public class AuthController : Controller
{
    private readonly IAuthService _authService;

    public AuthController(IAuthService authService)
    {
        _authService = authService;
    }

    public async Task<IActionResult> Add(UserViewModel model)
    {
        if (!ModelState.IsValid) return View(model);

        await _authService.CreateUser(model);
        
        TempData["Notification"] = "Registration successful! Welcome, " + model.Username + ".";
            
        return RedirectToPage("/Authenticate");
    }
}