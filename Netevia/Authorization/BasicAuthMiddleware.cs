﻿using System.Net.Http.Headers;
using System.Text;
using Netevia.Features.Interfaces;
using Netevia.Models;

namespace Netevia.Authorization;

public class BasicAuthMiddleware
{
    private readonly RequestDelegate _next;

    public BasicAuthMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context, IAuthService authService)
    {
        try
        {
            var authHeader = AuthenticationHeaderValue.Parse(context.Request.Headers["Authorization"]);
            var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
            var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':', 2);
            var username = credentials[0];
            var password = credentials[1];

            context.Items["User"] = await authService.FindUser(new UserViewModel()
            {
                Username = username,
                Password = password
            });
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

        await _next(context);
    }
}