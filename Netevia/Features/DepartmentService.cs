﻿using Microsoft.EntityFrameworkCore;
using Netevia.Database;
using Netevia.Features.Interfaces;
using Netevia.Models;

namespace Netevia.Features;

public class DepartmentService : IDepartmentService
{
    private readonly AppDbContext _context;

    public DepartmentService(AppDbContext context)
    {
        _context = context;
    }

    public async Task<List<DepartmentViewModel>> GetAll()
    {
        var data = await _context.Departments
            .Where(x=>!x.IsDelete)
            .Select(x => new DepartmentViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Floor = x.Floor
            })
            .ToListAsync();
        return data;
    }
}