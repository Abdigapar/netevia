﻿using Netevia.Models;

namespace Netevia.Features.Interfaces;

public interface IEmployeeService
{
    Task<List<EmployeeViewModel>> GetAll(string search);
    Task<EditEmployeeViewModel> GetById(int id);
    Task Add(AddEmployeeModel model);
    Task<EditEmployeeViewModel> Edit(int id, EditEmployeeViewModel model);
    Task Delete(int id);
}