﻿using Netevia.Models;

namespace Netevia.Features.Interfaces;

public interface IDepartmentService
{
    Task<List<DepartmentViewModel>> GetAll();
}