﻿using Netevia.Models;

namespace Netevia.Features.Interfaces;

public interface IAuthService
{
    Task<bool> FindUser(UserViewModel model);
    Task CreateUser(UserViewModel model);
}