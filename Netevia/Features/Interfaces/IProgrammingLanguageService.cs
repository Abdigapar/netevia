﻿using Netevia.Models;

namespace Netevia.Features.Interfaces;

public interface IProgrammingLanguageService
{
    Task<List<ProgrammingLanguageViewModel>> GetAll();
}