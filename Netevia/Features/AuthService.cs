﻿using Microsoft.EntityFrameworkCore;
using Netevia.Database;
using Netevia.Domain;
using Netevia.Features.Interfaces;
using Netevia.Models;

namespace Netevia.Features;

public class AuthService : IAuthService
{
    private readonly AppDbContext _context;

    public AuthService(AppDbContext context)
    {
        _context = context;
    }

    public async Task<bool> FindUser(UserViewModel model)
    {
        var user = await _context.Users
            .FirstOrDefaultAsync(u => u.Username == model.Username 
                                      && u.Password == model.Password);
        return user is not null;
    }

    public async Task CreateUser(UserViewModel model)
    {
        var newUser = new User()
        {
            IsDelete = false,
            Username = model.Username,
            Password = model.Password,
        };

        await _context.Users.AddAsync(newUser);
        await _context.SaveChangesAsync();
    }
}