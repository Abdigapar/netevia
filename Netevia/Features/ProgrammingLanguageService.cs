﻿using Microsoft.EntityFrameworkCore;
using Netevia.Database;
using Netevia.Features.Interfaces;
using Netevia.Models;

namespace Netevia.Features;

public class ProgrammingLanguageService : IProgrammingLanguageService
{
    private readonly AppDbContext _context;

    public ProgrammingLanguageService(AppDbContext context)
    {
        _context = context;
    }
    
    public async Task<List<ProgrammingLanguageViewModel>> GetAll()
    {
        var data = await _context.ProgrammingLanguages
            .Where(x=>!x.IsDelete)
            .Select(x => new ProgrammingLanguageViewModel()
            {
                Id= x.Id,
                Name = x.Name,
            })
            .ToListAsync();
        return data;
    }
}