﻿using Microsoft.EntityFrameworkCore;
using Netevia.Database;
using Netevia.Domain;
using Netevia.Features.Interfaces;
using Netevia.Models;

namespace Netevia.Features;

public class EmployeeService : IEmployeeService
{
    private readonly AppDbContext _context;

    public EmployeeService(AppDbContext context)
    {
        _context = context;
    }

    public async Task<List<EmployeeViewModel>> GetAll(string search)
    {
        var query = _context.Employees
            .Where(s => s.IsDelete == false)
            .AsNoTracking();

        if (!string.IsNullOrEmpty(search))
        {
            query = query.Where(s => s.IsDelete == false &&
                                     s.Id.ToString().Contains(search) ||
                                     s.FirstName.ToLower().Contains(search) ||
                                     s.LastName.ToLower().Contains(search) ||
                                     (s.FirstName + " " + s.LastName).ToLower().Contains(search));
        }

        var data = await query
            .Include(x => x.Department)
            .Include(x => x.Experiences)
            .Select(x => new EmployeeViewModel()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Age = x.Age,
                Gender = x.Gender,
                Department = x.Department != null
                    ? new DepartmentViewModel()
                    {
                        Id = x.Department.Id,
                        Floor = x.Department.Floor,
                        Name = x.Department.Name
                    }
                    : null
            })
            .ToListAsync();

        return data;
    }

    public async Task<EditEmployeeViewModel> GetById(int id)
    {
        var employee = await _context.Employees
            .Where(x=>x.Id == id)
            .Include(x=>x.Department)
            .Include(x=>x.Experiences)
            .FirstOrDefaultAsync();
        if (employee is null)
            throw new Exception("Employee Not found");

        return new EditEmployeeViewModel()
        {
            Id = employee.Id,
            FirstName = employee.FirstName,
            LastName = employee.LastName,
            Age = employee.Age,
            Gender = employee.Gender,
            DepartmentId = employee.DepartmentId,
            ExperienceIds = employee.Experiences.Select(x=>x.Id).ToList()
        };
    }

    public async Task Add(AddEmployeeModel model)
    {
        var employee = new Employee()
        {
            FirstName = model.FirstName,
            LastName = model.LastName,
            DepartmentId = model.DepartmentId,
            Gender = model.Gender,
            Age = model.Age,
            IsDelete = false
        };

        await _context.Employees.AddAsync(employee);
        await _context.SaveChangesAsync();

        await AddExperience(employee.Id, model.ExperienceIds);

    }

    private async Task AddExperience(int employeeId, List<int> experienceIds)
    {
        var experiences = experienceIds
            .Select(programmingLanguageId => new Experience()
            {
                IsDelete = false, 
                EmployeeId = employeeId,
                ProgrammingLanguageId = programmingLanguageId
            }).ToList();

        if (experiences.Any())
        {
            await _context.Experiences.AddRangeAsync(experiences);
            await _context.SaveChangesAsync();
        }
    }

    public async Task<EditEmployeeViewModel> Edit(int id, EditEmployeeViewModel model)
    {
        var employee = await _context.Employees.FindAsync(id);
        if (employee is null)
            throw new Exception("Employee Not found");

        employee.Age = model.Age;
        employee.DepartmentId = model.DepartmentId;
        employee.FirstName = model.FirstName;
        employee.LastName = model.LastName;
        employee.Gender = model.Gender;
        _context.Employees.Update(employee);
        await _context.SaveChangesAsync();

        return model;
    }

    public async Task Delete(int id)
    {
        var employee = await _context.Employees.FindAsync(id);
        if (employee is null)
            throw new Exception("Employee Not found");

        employee.IsDelete = true;
        _context.Employees.Update(employee);
        await _context.SaveChangesAsync();
    }
}